package autotrade.dg.autotradeforbinance.utils;

import com.google.gson.Gson;

import java.util.Random;

public class Utils {
    public static Gson gson = new Gson();

    public static float getMarginChanged(String numberInText1, String numberInText2) {
        try {
            float number1 = Float.parseFloat(numberInText1);
            float number2 = Float.parseFloat(numberInText2);

            return ((number2/number1) - 1) * 100;
        }catch (Exception ex) {
            ex.printStackTrace();
            return 0f;
        }
    }

    public static boolean isTimeToCheat() {
        Random r = new Random();
        int theRandomNumber = r.nextInt(10);

        if (theRandomNumber > 7) {
            return false;
        }

        return true;
    }
}
