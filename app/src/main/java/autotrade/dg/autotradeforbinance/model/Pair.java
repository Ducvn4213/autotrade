package autotrade.dg.autotradeforbinance.model;

import com.google.gson.annotations.SerializedName;

public class Pair {
    @SerializedName("id")
    public String id;
    @SerializedName("name")
    public String name;

    public PairInfo toPairInfo() {
        PairInfo pairInfo = new PairInfo();
        pairInfo.name = name;
        pairInfo.value = "0";

        return pairInfo;
    }
}
