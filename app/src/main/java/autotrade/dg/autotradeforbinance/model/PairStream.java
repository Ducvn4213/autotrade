package autotrade.dg.autotradeforbinance.model;

import com.google.gson.annotations.SerializedName;

public class PairStream {
    @SerializedName("s")
    public String name;
    @SerializedName("q")
    public String volume;
    @SerializedName("o")
    public String open;
    @SerializedName("c")
    public String close;
    @SerializedName("h")
    public String high;
    @SerializedName("l")
    public String low;

    public PairStream(String name) {
        this.name = name;
        this.volume = "0";
        this.open = "0";
        this.close = "0";
        this.high = "0";
        this.low = "0";
    }

    public PairInfo toPairInfo() {
        PairInfo pairInfo = new PairInfo();
        pairInfo.name = name.replace("ETH", "");
        pairInfo.value = close;

        return pairInfo;
    }

    public String getName() {
        return name.replace("ETH", "");
    }
}
