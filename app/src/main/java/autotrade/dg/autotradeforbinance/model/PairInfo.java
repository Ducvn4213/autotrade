package autotrade.dg.autotradeforbinance.model;

/**
 * Created by DG4213 on 2/13/18.
 */

public class PairInfo {
    public String name;
    public String value;

    public PairInfo(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public PairInfo() {}

    public String getStreamName() {
        return name.toLowerCase() + "eth@ticker";
    }
}
