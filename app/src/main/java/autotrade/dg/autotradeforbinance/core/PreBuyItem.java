package autotrade.dg.autotradeforbinance.core;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static autotrade.dg.autotradeforbinance.core.AutoTradeCore.TRADE_INFO_NEW_RESPONSE;
import static autotrade.dg.autotradeforbinance.core.AutoTradeCore.TRADE_INFO_PROCESS_RESPONSE;

public class PreBuyItem {
    public String key;
    public String price;
    public float volume;

    public PreBuyItem(String key, String price, float volume) {
        this.key = key;
        this.price = price;
        this.volume = volume;
    }

    public boolean isLessThan(PreBuyItem item) {
        if (this.volume < item.volume) {
            return true;
        }

        return false;
    }
}
