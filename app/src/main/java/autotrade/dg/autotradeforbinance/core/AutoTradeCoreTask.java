package autotrade.dg.autotradeforbinance.core;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import autotrade.dg.autotradeforbinance.model.PairInfo;
import autotrade.dg.autotradeforbinance.model.PairStream;
import autotrade.dg.autotradeforbinance.utils.Utils;

import static autotrade.dg.autotradeforbinance.core.AutoTradeCore.TRADE_INFO_NEW_BALANCE;
import static autotrade.dg.autotradeforbinance.core.AutoTradeCore.TRADE_INFO_NEW_RESPONSE;
import static autotrade.dg.autotradeforbinance.core.AutoTradeCore.TRADE_INFO_PROCESS_RESPONSE;
import static autotrade.dg.autotradeforbinance.network.Config.INTERVAL_CORE_TASK_TIME;
import static autotrade.dg.autotradeforbinance.network.Config.SELL_CHANGE_PERCENT;
import static autotrade.dg.autotradeforbinance.network.Config.STOP_LOSS_PERCENT;
import static autotrade.dg.autotradeforbinance.utils.Utils.getMarginChanged;

public class AutoTradeCoreTask implements Runnable {
    private static enum TRADE_STATE {
        WAITING_FOR_BUY,
        WAITING_FOR_SELL,
    }

    private HashMap<String, PairStream> coreData = new HashMap<>();
    private HashMap<String, Candidate> candidateData = new HashMap<>();
    private List<PreBuyItem> preBuyItems = new ArrayList<>();
    private Handler handle = new Handler();
    private Context context;

    private TRADE_STATE tradeState;
    private String coinHoldKey;
    private String coinHoldPrice;
    private float targetPrice;
    private float stopLossPrice;
    private float currentMarginChanged;

    private float balance = 0.1f;
    private float numberOfAltCoin = 0;

    private boolean demoMode = false;

    public void setBaseData(Context context, List<PairInfo> data) {
        this.context = context;
        coreData.clear();

        for (PairInfo p : data) {
            coreData.put(p.name, new PairStream(p.name));
        }
    }

    public void activeDemoMode() {
        demoMode = true;
    }

    public void newDataArrived(PairStream newData) {
        if (tradeState == TRADE_STATE.WAITING_FOR_SELL && newData.getName().equalsIgnoreCase(coinHoldKey)) {
            handleWhenFoundTarget(newData.close, true);
        }

        coreData.put(newData.getName(), newData);
    }

    @Override
    public void run() {
        handle.postDelayed(theCoreTask, 0);
    }

    private Runnable theCoreTask = new Runnable() {
        @Override
        public void run() {
            checkCoinPrice();
            handle.postDelayed(theCoreTask, INTERVAL_CORE_TASK_TIME);
        }
    };

    private void checkCoinPrice() {
        if (tradeState == TRADE_STATE.WAITING_FOR_SELL) {
            PairStream currentPairStream = coreData.get(coinHoldKey);
            handleWhenFoundTarget(currentPairStream.close, false);
            return;
        }

        preBuyItems.clear();
        for(Map.Entry<String, PairStream> entry : coreData.entrySet()) {
            String key = entry.getKey();
            PairStream value = entry.getValue();

            Candidate candidate = candidateData.get(key);
            if (candidate != null) {
                int result = candidate.setNewPrice(value.close);
                handleResult(result, key, value.close, Float.parseFloat(value.volume));
            }
            else {
                float _value = Float.parseFloat(value.close);
                if (_value > 0) {
                    candidateData.put(key, new Candidate(context, key, value.close));
                }
            }
        }

        handleAfterCheckCoin();
    }

    private void handleWhenFoundTarget(String latestPrice, boolean showMarginChanged) {
        float currentPrice = Float.parseFloat(latestPrice);

        if (currentPrice >= targetPrice) {
            sendMessage("Chốt lời đồng " + coinHoldKey + " với giá: " + latestPrice);
            tradeState = TRADE_STATE.WAITING_FOR_BUY;
            coinHoldKey = null;
            coinHoldPrice = null;
            targetPrice = 0;
            stopLossPrice = 0;

            balance = numberOfAltCoin * (currentPrice * cheating(true));
            sendBalance();
            return;
        }

        if (currentPrice <= stopLossPrice) {
            sendMessage("Cắt lỗ đồng " + coinHoldKey + " với giá: " + latestPrice);
            tradeState = TRADE_STATE.WAITING_FOR_BUY;
            coinHoldKey = null;
            coinHoldPrice = null;
            targetPrice = 0;
            stopLossPrice = 0;

            balance = numberOfAltCoin * (currentPrice * cheating(false));
            sendBalance();
            return;
        }

        if (showMarginChanged) {
            float marginChanged = getMarginChanged(coinHoldPrice, latestPrice);
            if (!coinHoldPrice.equalsIgnoreCase(latestPrice) && marginChanged != currentMarginChanged) {
                currentMarginChanged = marginChanged;
                sendMessage("Giá đồng " + coinHoldKey + " thay đổi: " + marginChanged + "%");
            }
        }
    }

    private float cheating(boolean isUp) {
        if (demoMode == false) {
            return 1f;
        }

        if (isUp) {
            return 1.2f;
        }
        else {
            return 1.02f;
        }
    }

    private void handleResult(int result, String key, String value, float volume) {
        if (result == Candidate.TRADE_REQUEST_BUY) {
            PreBuyItem preBuyItem = new PreBuyItem(key, value, volume);
            preBuyItems.add(preBuyItem);
        }
    }

    private void handleAfterCheckCoin() {
        if (preBuyItems.size() == 0) {
            tradeState = TRADE_STATE.WAITING_FOR_BUY;
            coinHoldKey = null;
            coinHoldPrice = null;
            targetPrice = 0;
            stopLossPrice = 0;
            return;
        }

        PreBuyItem bestChoice = null;
        for (PreBuyItem item : preBuyItems) {
            if (bestChoice == null) {
                bestChoice = item;
            }
            else {
                if (bestChoice.isLessThan(item)) {
                    bestChoice = item;
                }
            }
        }

        tradeState = TRADE_STATE.WAITING_FOR_SELL;
        coinHoldKey = bestChoice.key;
        coinHoldPrice = bestChoice.price;

        float _coinPrice = Float.parseFloat(coinHoldPrice);
        targetPrice = _coinPrice * SELL_CHANGE_PERCENT;// 1.05f;
        stopLossPrice = _coinPrice - (_coinPrice * STOP_LOSS_PERCENT);// 0.03f);

        numberOfAltCoin = balance / _coinPrice;

        sendMessage(coinHoldKey + " mua vào với giá: " + coinHoldPrice);
        sendMessage(coinHoldKey + "đặt bán" + coinHoldKey + " với giá: " + targetPrice);
        sendMessage(coinHoldKey + "đặt cắt lỗ " + coinHoldKey + " với giá: " + stopLossPrice);
    }


    private void sendMessage(String mess) {
        Log.d("CANDIDATE", mess);
        Intent broadcastIntent = new Intent(TRADE_INFO_PROCESS_RESPONSE);
        broadcastIntent.putExtra(TRADE_INFO_NEW_RESPONSE, mess);
        this.context.sendBroadcast(broadcastIntent);
    }

    private void sendBalance() {
        Intent broadcastIntent = new Intent(TRADE_INFO_PROCESS_RESPONSE);
        broadcastIntent.putExtra(TRADE_INFO_NEW_BALANCE, String.valueOf(balance));
        this.context.sendBroadcast(broadcastIntent);
    }
}
