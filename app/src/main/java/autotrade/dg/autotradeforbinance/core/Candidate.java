package autotrade.dg.autotradeforbinance.core;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import static autotrade.dg.autotradeforbinance.core.AutoTradeCore.TRADE_INFO_NEW_RESPONSE;
import static autotrade.dg.autotradeforbinance.core.AutoTradeCore.TRADE_INFO_PROCESS_RESPONSE;

public class Candidate {

    public static final int TRADE_RESET = 000;
    public static final int TRADE_UP_ONE_TIME = 555;
    public static final int TRADE_REQUEST_BUY = 999;

    private Context context;
    private String key;
    private String price_1;
    private String price_2;
    private String price_3;

    public Candidate(Context context, String key, String price) {
        this.context = context;
        this.key = key;
        this.price_1 = price;
    }

    public int setNewPrice(String price) {
        if (price_2 == null) {
            price_2 = price;
            price_3 = null;
            return analytics();
        }

        if (price_3 == null) {
            price_3 = price;
        }

        return analytics();
    }

    private int analytics() {
        float price1 = Float.parseFloat(price_1);
        float price2 = Float.parseFloat(price_2);

        if (price1 >= price2) {
            sendMessage(key + " mất đà, reset.");
            price_1 = price_2;
            price_2 = null;
            price_3 = null;
            return TRADE_RESET;
        }

        if (price_3 == null) {
            sendMessage(key + " đang có đà 1 lần.");
            return TRADE_UP_ONE_TIME;
        }

        float price3 = Float.parseFloat(price_3);
        if (price2 >= price3) {
            sendMessage(key + " mất đà, reset.");
            price_1 = price_3;
            price_2 = null;
            price_3 = null;
            return TRADE_RESET;
        }

        //sendMessage(key + " mua vào với giá: " + price_3);
        return TRADE_REQUEST_BUY;
    }

    private void sendMessage(String mess) {
        Log.d("CANDIDATE", mess);
        Intent broadcastIntent = new Intent(TRADE_INFO_PROCESS_RESPONSE);
        broadcastIntent.putExtra(TRADE_INFO_NEW_RESPONSE, mess);
        this.context.sendBroadcast(broadcastIntent);
    }
}
