package autotrade.dg.autotradeforbinance.core;

import android.content.Context;

import java.util.List;

import autotrade.dg.autotradeforbinance.model.PairInfo;
import autotrade.dg.autotradeforbinance.model.PairStream;

public class AutoTradeCore {

    public static final String TRADE_INFO_PROCESS_RESPONSE = "com.auto.trade.TRADE_INFO_PROCESS_RESPONSE";
    public static final String TRADE_INFO_NEW_RESPONSE = "com.auto.trade.TRADE_INFO_NEW_RESPONSE";
    public static final String TRADE_INFO_NEW_BALANCE = "com.auto.trade.TRADE_INFO_NEW_BALANCE";

    private Context context;
    private Boolean demoMode = false;

    private static AutoTradeCore instance;
    private AutoTradeCore(Context context) {
        this.context = context;
    }

    private Thread intervalThread;
    private AutoTradeCoreTask autoTradeCoreTask = new AutoTradeCoreTask();

    public static AutoTradeCore getInstance(Context context) {
        if (instance == null) {
            instance = new AutoTradeCore(context);
        }

        return instance;
    }

    public void activeDemoMode() {
        demoMode = true;
    }

    public void disableDemoMode() {
        demoMode = false;
    }

    public void setBaseData(List<PairInfo> data) {
        autoTradeCoreTask.setBaseData(context, data);
        autoTradeCoreTask.activeDemoMode();
        startIntervalTask();
    }

    private void startIntervalTask() {
        intervalThread = new Thread(autoTradeCoreTask);
        intervalThread.start();
    }

    public void newDataArrived(PairStream newData) {
        autoTradeCoreTask.newDataArrived(newData);
    }
}
