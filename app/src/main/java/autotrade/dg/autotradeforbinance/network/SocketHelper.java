package autotrade.dg.autotradeforbinance.network;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

import java.util.List;

import autotrade.dg.autotradeforbinance.model.PairInfo;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

import static autotrade.dg.autotradeforbinance.network.Config.INTERVAL_RESET_SOCKET_TIME;

public class SocketHelper {

    public static final String PRICE_INFO_PROCESS_RESPONSE = "com.auto.trade.PRICE_INFO_PROCESS_RESPONSE";
    public static final String PRICE_INFO_NEW_RESPONSE = "com.auto.trade.PRICE_INFO_NEW_RESPONSE";

    private static final int NORMAL_CLOSURE_STATUS = 1000;

    private static SocketHelper instance;
    private SocketHelper() {}

    public static SocketHelper getInstance() {
        if (instance == null) {
            instance = new SocketHelper();
        }

        return instance;
    }

    private Handler getPriceInfoHandler = new Handler();
    private GetPriceInfoTask getPriceInfoTask = new GetPriceInfoTask();

    public void start(final Context context, final String APIKEY, List<PairInfo> data) {
        getPriceInfoHandler.removeCallbacks(getPriceInfoTask);

        getPriceInfoTask.setPairs(context, APIKEY, data);
        getPriceInfoHandler.post(getPriceInfoTask);
    }

    private class GetPriceInfoTask implements Runnable {
        private final int CLOSE_SOCKET_CODE = 1000;
        private final String CLOSE_SOCKET_REASON = "CLOSE_SOCKET_REASON";

        private List<PairInfo> pairs;
        private String apiKey;
        private Context context;
        private WebSocket webSocket;

        public void setPairs(Context context, String apiKey, List<PairInfo> pairs) {
            this.context = context;
            this.apiKey = apiKey;
            this.pairs = pairs;
        }

        @Override
        public void run() {
            if (webSocket != null) {
                webSocket.close(CLOSE_SOCKET_CODE, CLOSE_SOCKET_REASON);
            }

            String baseLink = Config.SOCKET_API_HOST;

            for (PairInfo p : pairs) {
                baseLink += p.getStreamName() + "/";
            }

            baseLink = baseLink.substring(0, baseLink.length() - 1);

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            OkHttpClient client = builder.build();
            Request request = new Request.Builder()
                    .url(baseLink)
                    .header("Auth-Token", apiKey)
                    .build();

            webSocket = client.newWebSocket(request, new WebSocketListener() {
                @Override
                public void onOpen(WebSocket webSocket, Response response) {
                    webSocket.send("{Auth-Token:" + apiKey + "}");
                }

                @Override
                public void onMessage(WebSocket webSocket, String text) {
                    //Log.e("SOCKET_HELPER", text);
                    Intent broadcastIntent = new Intent(PRICE_INFO_PROCESS_RESPONSE);
                    broadcastIntent.putExtra(PRICE_INFO_NEW_RESPONSE, text);
                    context.sendBroadcast(broadcastIntent);
                }

                @Override
                public void onMessage(WebSocket webSocket, ByteString bytes) {}

                @Override
                public void onClosing(WebSocket webSocket, int code, String reason) {
                    webSocket.close(NORMAL_CLOSURE_STATUS, null);
                }

                @Override
                public void onFailure(WebSocket webSocket, Throwable t, Response response) {}
            });

            getPriceInfoHandler.postDelayed(this, INTERVAL_RESET_SOCKET_TIME);
        }
    };
}
