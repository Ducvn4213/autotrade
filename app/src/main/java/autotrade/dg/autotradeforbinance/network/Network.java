package autotrade.dg.autotradeforbinance.network;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Network {

    private MediaType CONTENT_TYPE = MediaType.parse("application/x-www-form-urlencoded;charset=utf-8");

    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .build();

    public interface Callback {
        public void onCallBack(String response);
        public void onFail(String error);
    }

    private static Network instance;
    private Network() {}

    public static Network getInstance() {
        if (instance == null) {
            instance = new Network();
        }
        return instance;
    }

    public void executeGet(String link, final Callback callback) {
        Request request = new Request.Builder()
                .url(link)
                .method("GET", null)
                .build();

        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                callback.onFail(e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {
                if (response.code() != 200) {
                    callback.onFail("network fail");
                    return;
                }

                String body = response.body().string();
                callback.onCallBack(body);
            }
        });
    }
}
