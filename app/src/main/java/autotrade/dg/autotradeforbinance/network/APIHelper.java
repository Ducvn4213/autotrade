package autotrade.dg.autotradeforbinance.network;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;

import autotrade.dg.autotradeforbinance.model.Pair;
import autotrade.dg.autotradeforbinance.utils.Utils;

public class APIHelper {

    public interface GetPairListCallback {
        void onSuccess(List<Pair> data);
        void onFail();
    }

    private static APIHelper instance;
    private APIHelper() {}

    public static APIHelper getInstance() {
        if (instance == null) {
            instance = new APIHelper();
        }

        return instance;
    }

    private Network mNetwork = Network.getInstance();

    public void getPairList(final GetPairListCallback callback) {
        String link = Config.WEB_API_HOST;
        mNetwork.executeGet(link, new Network.Callback() {
            @Override
            public void onCallBack(String response) {
                try {
                    List<Pair> pairInfos = Utils.gson.fromJson(response, new TypeToken<List<Pair>>(){}.getType());
                    callback.onSuccess(pairInfos);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    callback.onFail();
                }
            }

            @Override
            public void onFail(String error) {
                callback.onFail();
            }
        });
    }
}
