package autotrade.dg.autotradeforbinance.network;

public class Config {
    public static final String WEB_API_HOST = "http://baggaarm.tk";
    public static final String SOCKET_API_HOST = "wss://stream.binance.com:9443/ws/";

    public static final int INTERVAL_CORE_TASK_TIME = 1 * 60 * 1000;
    public static final int INTERVAL_RESET_SOCKET_TIME = 3 * 3600 * 1000;
    public static final float SELL_CHANGE_PERCENT = 1.02f;
    public static final float STOP_LOSS_PERCENT = 0.03f;
}
