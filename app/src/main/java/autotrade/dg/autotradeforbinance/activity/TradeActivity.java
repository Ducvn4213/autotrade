package autotrade.dg.autotradeforbinance.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import autotrade.dg.autotradeforbinance.adapter.PairAdapter;
import autotrade.dg.autotradeforbinance.R;
import autotrade.dg.autotradeforbinance.core.AutoTradeCore;
import autotrade.dg.autotradeforbinance.model.Pair;
import autotrade.dg.autotradeforbinance.model.PairInfo;
import autotrade.dg.autotradeforbinance.model.PairStream;
import autotrade.dg.autotradeforbinance.network.APIHelper;
import autotrade.dg.autotradeforbinance.network.SocketHelper;
import autotrade.dg.autotradeforbinance.utils.Utils;

public class TradeActivity extends AppCompatActivity {

    private TextView mTradeInfo;
    private TextView mBalance;

    private GridView mGridView;
    private PairAdapter mAdapter;

    private APIHelper mApiHelper = APIHelper.getInstance();
    private SocketHelper mSocketHelper = SocketHelper.getInstance();
    private AutoTradeCore mAutoTradeCode;

    private StreamDataReceiver mPriceInfoReceiver = new StreamDataReceiver();
    private TradeDataReceiver mTradeInfoReceiver = new TradeDataReceiver();

    private IntentFilter mPriceInfoFilter = new IntentFilter(SocketHelper.PRICE_INFO_PROCESS_RESPONSE);
    private IntentFilter mTradeInfoFilter = new IntentFilter(AutoTradeCore.TRADE_INFO_PROCESS_RESPONSE);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trade);

        mAutoTradeCode = AutoTradeCore.getInstance(TradeActivity.this);
        mAutoTradeCode.activeDemoMode();

        bindControls();
        initBaseData();
        loadData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        registerReceiver(mPriceInfoReceiver, mPriceInfoFilter);
        registerReceiver(mTradeInfoReceiver, mTradeInfoFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(mPriceInfoReceiver);
        unregisterReceiver(mTradeInfoReceiver);
    }

    private void bindControls() {
        mGridView = (GridView) findViewById(R.id.gv_pairs);
        mTradeInfo = (TextView) findViewById(R.id.tv_trade_info);
        mBalance = (TextView) findViewById(R.id.tv_balance);
    }

    private void initBaseData() {
        mPriceInfoFilter.addCategory(Intent.CATEGORY_DEFAULT);

        mAdapter = new PairAdapter(TradeActivity.this);
        mGridView.setAdapter(mAdapter);
    }

    private void loadData() {
        mApiHelper.getPairList(new APIHelper.GetPairListCallback() {
            @Override
            public void onSuccess(List<Pair> data) {
                List<PairInfo> pairInfoList = new ArrayList<>();

                if (data != null) {
                    for (Pair p : data) {
                        pairInfoList.add(p.toPairInfo());
                    }
                }

                handleAfterGetPairList(pairInfoList);
            }

            @Override
            public void onFail() {
                //TODO
            }
        });
    }

    private void handleAfterGetPairList(final List<PairInfo> data) {
        TradeActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.setNewData(data);
                mAdapter.notifyDataSetChanged();

                mAutoTradeCode.setBaseData(data);
                mSocketHelper.start(TradeActivity.this, "0mjlpZXay8a6WkzeqMh6bhQqg3D24T596qwdb16Yw0ZRa2X6eTGP7NR3wxCXgTdu", data);
            }
        });
    }

    private class TradeDataReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String responseString = intent.getStringExtra(AutoTradeCore.TRADE_INFO_NEW_RESPONSE);
            String responseBalanceString = intent.getStringExtra(AutoTradeCore.TRADE_INFO_NEW_BALANCE);

            handleNewTradeInfo(responseString, responseBalanceString);
        }
    }

    private class StreamDataReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String responseString = intent.getStringExtra(SocketHelper.PRICE_INFO_NEW_RESPONSE);
            handleNewPriceData(responseString);
        }
    }

    private void handleNewPriceData(String data) {
        PairStream pairStream = Utils.gson.fromJson(data, PairStream.class);

        mAutoTradeCode.newDataArrived(pairStream);
        mAdapter.setNewDataValue(pairStream.toPairInfo());
    }

    private void handleNewTradeInfo(String responseString, String responseBalanceString) {

        if (responseString == null) {
            mBalance.setText("Đang có: " + responseBalanceString + " ETH");
            return;
        }

        mTradeInfo.append(responseString + "\n");
        final int scrollAmount = mTradeInfo.getLayout().getLineTop(mTradeInfo.getLineCount()) - mTradeInfo.getHeight();
        if (scrollAmount > 0)
            mTradeInfo.scrollTo(0, scrollAmount);
        else
            mTradeInfo.scrollTo(0, 0);
    }
}
