package autotrade.dg.autotradeforbinance.activity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import autotrade.dg.autotradeforbinance.R;
import pl.droidsonroids.gif.GifImageView;

public class HireActivity extends Activity {

    TextView theAddressTextView;
    TextView needConfirmTextView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hire);

        bindingControls();
        setupControlEvents();
    }

    private void bindingControls() {
        theAddressTextView = (TextView) findViewById(R.id.tv_the_address);
        needConfirmTextView = (TextView) findViewById(R.id.tv_need_confirm);
    }

    private void setupControlEvents() {
        theAddressTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyToClipBoard();
            }
        });

        needConfirmTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HireActivity.this, PurchaseConfirmActivity.class);
                HireActivity.this.startActivity(intent);
            }
        });
    }

    private void copyToClipBoard() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(theAddressTextView.getText().toString(), theAddressTextView.getText().toString());
        clipboard.setPrimaryClip(clip);
        showToast(getString(R.string.copy_completed));
    }

    private void showToast(String message) {
        Toast.makeText(HireActivity.this, message, Toast.LENGTH_SHORT).show();
    }
}
