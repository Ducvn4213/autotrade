package autotrade.dg.autotradeforbinance.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import autotrade.dg.autotradeforbinance.R;
import autotrade.dg.autotradeforbinance.view.RoundButtonWithTitle;

public class BuyLicenseInfoActivity extends Activity {

    RoundButtonWithTitle hireButton;
    RoundButtonWithTitle buyButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_license_info);

        bindingControls();
        configUI();
        setupControlEvents();
    }

    private void bindingControls() {
        hireButton = (RoundButtonWithTitle) findViewById(R.id.btn_hire);
        buyButton = (RoundButtonWithTitle) findViewById(R.id.btn_buy);
    }

    private void configUI() {
        hireButton.init(R.string.button_hire, R.drawable.ic_rent, null);
        buyButton.init(R.string.button_buy, R.drawable.ic_buy, null);
    }

    private void setupControlEvents() {
        hireButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(BuyLicenseInfoActivity.this, HireActivity.class);
                BuyLicenseInfoActivity.this.startActivity(intent);
                BuyLicenseInfoActivity.this.finish();
            }
        });

        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(BuyLicenseInfoActivity.this, BuyActivity.class);
                BuyLicenseInfoActivity.this.startActivity(intent);
                BuyLicenseInfoActivity.this.finish();
            }
        });
    }
}
