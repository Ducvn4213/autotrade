package autotrade.dg.autotradeforbinance.activity;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import autotrade.dg.autotradeforbinance.R;

public class PurchaseConfirmActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_confirm);

        bindingControls();
        setupControlEvents();
    }

    private void bindingControls() {
    }

    private void setupControlEvents() {
    }
}
