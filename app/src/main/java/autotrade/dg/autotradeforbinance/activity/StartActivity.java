package autotrade.dg.autotradeforbinance.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import autotrade.dg.autotradeforbinance.R;
import pl.droidsonroids.gif.GifImageView;

public class StartActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
    }

    @Override
    protected void onResume() {
        super.onResume();

        GifImageView mGifPlayer = (GifImageView) findViewById(R.id.giv_gif_player);
        mGifPlayer.animate().alpha(0).setDuration(300).setStartDelay(2200);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                StartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        goToMain();
                    }
                });
            }
        }, 2800);
    }

    private void goToMain() {
        Intent intent = new Intent(StartActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
