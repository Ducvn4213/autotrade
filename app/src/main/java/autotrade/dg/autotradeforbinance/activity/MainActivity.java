package autotrade.dg.autotradeforbinance.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import autotrade.dg.autotradeforbinance.R;
import autotrade.dg.autotradeforbinance.view.RoundButtonWithTitle;

public class MainActivity extends Activity {

    RoundButtonWithTitle demoButton;
    RoundButtonWithTitle buyLicenseButton;
    RoundButtonWithTitle startButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindingControls();
        configUI();
        setupControlEvents();
    }

    private void bindingControls() {
        demoButton = (RoundButtonWithTitle) findViewById(R.id.btn_demo);
        buyLicenseButton = (RoundButtonWithTitle) findViewById(R.id.btn_buy_license);
        startButton = (RoundButtonWithTitle) findViewById(R.id.btn_start);
    }

    private void configUI() {
        demoButton.init(R.string.button_demo, R.drawable.ic_trial, null);
        buyLicenseButton.init(R.string.button_buy_license, R.drawable.ic_buy_license, null);
        startButton.init(R.string.button_start, R.drawable.ic_play, null);
    }

    private void setupControlEvents() {
        demoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDemoButtonClick();
            }
        });

        buyLicenseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToBuyLicense();
            }
        });
    }

    private void onDemoButtonClick() {
        final android.support.v7.app.AlertDialog dialog = new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                .setTitle(R.string.demo_dialog_title)
                .setMessage(R.string.demo_dialog_message)
                .setNegativeButton(R.string.button_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.this.goToDemonstration();
                    }
                }).create();

        if (!dialog.isShowing()) {
            dialog.show();
        }
    }

    private void onBuyLicenseButtonClick() {
        //TODO
    }

    private void onStartButtonClick() {
        //TODO
    }

    private void goToDemonstration() {
        Intent intent = new Intent(MainActivity.this, TradeActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToBuyLicense() {
        Intent intent = new Intent(MainActivity.this, BuyLicenseInfoActivity.class);
        startActivity(intent);
    }
}
