package autotrade.dg.autotradeforbinance.dummy;

import java.util.ArrayList;
import java.util.List;

import autotrade.dg.autotradeforbinance.model.PairInfo;

public class DummyData {
    public static PairInfo[] pairsInfo() {
        List<PairInfo> returnValue = new ArrayList<>();

        PairInfo fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);
        fake_info = new PairInfo("ENJ", "0.00012348");
        returnValue.add(fake_info);

        return returnValue.toArray(new PairInfo[returnValue.size()]);
    }
}
