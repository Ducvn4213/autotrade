package autotrade.dg.autotradeforbinance.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import autotrade.dg.autotradeforbinance.R;

public class RoundButtonWithTitle extends LinearLayout {
    private ImageButton theButton;
    private TextView theTitle;

    public RoundButtonWithTitle(Context context) {
        this(context, null);
    }

    public RoundButtonWithTitle(Context context, AttributeSet attrs) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.view_round_button_with_title, this, true);

        bindingControls();
    }

    private void bindingControls() {
        theButton = (ImageButton) findViewById(R.id.btn_button);
        theTitle= (TextView) findViewById(R.id.tv_title);
    }

    public void init(int titleID, int imageID, OnClickListener onClickListener) {
        theTitle.setText(titleID);
        theButton.setImageDrawable(getContext().getDrawable(imageID));
        setOnClickListener(onClickListener);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        super.setOnClickListener(l);
        theButton.setOnClickListener(l);
    }
}
