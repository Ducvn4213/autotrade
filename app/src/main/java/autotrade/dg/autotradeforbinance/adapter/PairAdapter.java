package autotrade.dg.autotradeforbinance.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import autotrade.dg.autotradeforbinance.R;
import autotrade.dg.autotradeforbinance.model.Pair;
import autotrade.dg.autotradeforbinance.model.PairInfo;

public class PairAdapter extends BaseAdapter {

    private Context mContext;
    private List<PairInfo> mData;

    public PairAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return mData == null ? 0 : mData.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mData == null) {
            return null;
        }

        final PairInfo pairInfo = mData.get(position);

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.layout_pair_item, null);
        }

        final TextView name = (TextView)convertView.findViewById(R.id.tv_name);
        final TextView value = (TextView)convertView.findViewById(R.id.tv_value);

        name.setText(pairInfo.name);
        value.setText(pairInfo.value);

        return convertView;
    }

    public void setNewData(List<PairInfo> data) {
        this.mData = data;
        notifyDataSetChanged();
    }

    public void setNewDataValue(PairInfo dataValue) {
        for (PairInfo p : mData) {
            if (p.getStreamName().equalsIgnoreCase(dataValue.getStreamName())) {
                p.value = dataValue.value;

                notifyDataSetChanged();
                return;
            }
        }
    }
}
